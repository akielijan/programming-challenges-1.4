﻿# About challenge #
4chan surely is quite amusing community, isn't it? That list was posted in January, 2015, but I just found about it today (14th Nov 2016). 
So, just looked at it and - what can go wrong, why won't I just try it out? Well, I bet it wasn't my best choice.

edit March 17th: gave up in the mid of November. Well, happens. About 20th or so. Had more "important" things to finish, i.e. playing whole day long or learning RE.

Time to start it. 100 days for 100 tasks (actually 99, because it's impossible to do all the Project Euler's tasks within few days).

Rules:

-  up to two tasks per day
-  no minimal amount of tasks per day declared
-  100 days to finish all tasks from list

Check my programmer's diary [here](#markdown-header-programmers-diary)

# Complete list of tasks #
00. Name generator
01. Higher/lower, heads/tails
02. Temperature converter
03. Calculate age in seconds (+ leap years)
04. Encryption/decryption algorithm
05. ~~FizzBuzz~~ (14th Nov 2016)
06. Rock paper scissors (lizard Spock)
07. ~~Project Euler~~ (not gonna happen, not in 100 days)
08. ~~Hangman~~ (15th Nov 2016, partially finished)
09. Love calculator
10. Random sentence generator
11. Password generator
12. Internet time ([S]NTP)
13. Haiku generator
14. Magic eight ball
15. Collatz conjecture
16. ~~Reverse string~~ (14th Nov 2016)
17. Eulerian path
18. Simple file explorer
19. Count words in a string
20. ~~Minesweeper~~ (16th/17th Nov 2016)
21. Connect four
22. ~~BMI calculator~~ (20th Nov 2016)
23. ~~4chan thread downloader (/gif/ ( ͡° ͜ʖ ͡°))~~ (17th Nov 2016)
24. Sudoku generator/solver
25. Maze generator/solver
26. Radix (base) converter
27. Picross solver
28. Fibonacci sequence
29. ~~Factorial~~ (15th Nov, 2016)
30. Cipher encryption/decryption tool
31. Blackjack
32. D&D play with AI
33. ASCII tree generator
34. Genetic algorithms on polygons
35. Benford's Law
36. Currency converter
37. Static website generator
38. Crossword game
39. Scientific calculator
40. Perlin noise
41. Image viewer
42. ASCII digital clock
43. Dijkstra's algorithm
44. Text/Morse code translator with sounds
45. Tic-Tac-Toe
46. Snake game
47. FTP client
48. Telnet server
49. IMP interpreter
50. Tetris
51. Conway's Game of Life
52. Web crawler
53. Text editor
54. RSS feed reader
55. Reverse Polish Notation calculator
56. Mandelbrot set
57. Sorting algorithms + visualisation
58. Custom markup to HTML converter
59. N queens problem
60. Flow in a network (Ford-Fulkerson algorithm)
61. Credentials validator
62. Linked list (single & double)
63. Mastermind
64. Random image generator (gotta be crazy though)
65. Ulam spiral
66. Klingon translator
67. Prime numbers generator (sieve)
68. Markov chains (random text generator)
69. Graphical analog clock
70. C++/C# & Java communication
71. ~~Triangle number calculator~~ (18th Nov 2016)
72. Typing speed calculator (gotta be useful while making that stuff)
73. Name art in ASCII
74. Towers of Hanoi
75. ~~Quine~~ (19th Nov 2016)
76. IRC bot
77. ~~Brainfuck interpreter~~ (18th Nov 2016)
78. Knight's tour
79. Chip-8 emulator
80. Geekcode generator (3.12)
81. Define, translate, rotate a polygon
82. Pong with variable vectors
83. Battleships with AI
84. Simple paint program
85. TCP chat with basic encryption
86. Incremental economy simulator (*Time of Exploration*-like)
87. Encrypting data in images (steganography)
88. Pascal's triangle
89. Sine wave generator from pseudorandom numbers
90. Flappy bird
91. Fourier transform (FFT + visualisation)
92. Method ringing simulator
93. Binary search (n-ary)
94. Nintendo Oil Panic
95. Sierpiński triangle
96. Dot & cross product of two vectors
97. Little Man computer simulation
98. LISP interpreter
99. Enigma simulator with a config file (+ decrypt without known settings)

## Programmer's diary ##
#### 14th Nov, 2016 ####
Start of the project

Done tasks:

- FizzBuzz
- Reverse string

#### 15th Nov, 2016 ####
Started works @ ASCII digital clock and Hangman.

Done tasks:

- Factorial (BigInteger)
- Hangman (initial version with no hangman image + hardcoded list - prepared for JSON list parsing)

#### 16th Nov, 2016 ####
Quite hard day here. Tired as hell. Fixed small issue with Hangman.

Done tasks:

- Minesweeper (16th/17th Nov 2016)

#### 17th Nov, 2016 ####

Done tasks:

- Minesweeper
- 4Chan thread downloader

#### 18th Nov, 2016 ####

Done tasks:

- Brainfuck interpreter
- Triangle number (drawing is boring, maybe gonna implement canvas for that)

#### 19th Nov, 2016 ####

Done tasks:

- Quine (well that was fun)

#### 20th Nov, 2016 ####

Had no time to focus on challenge, so I did very fast implementation of BMI calculator. Just cm/kg fields with simple try block.

Done tasks:

- BMI