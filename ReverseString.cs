﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProgrammingChallenge
{
    class ReverseString
    {
        public static string reverse(string toReverse)
        {
            string reversed = "";
            for(int i=toReverse.Length; i>0;)
            {
                reversed += toReverse[--i];
            }
            return reversed;
        }
    }
}
