﻿using System;

namespace ProgrammingChallenge
{
    class FizzBuzz
    {
        /// <summary>
        /// Fizzbuzzs values from the specified start till the end (inclusive).
        /// </summary>
        /// <param name="start">The start.</param>
        /// <param name="end">The end.</param>
        public static void fizzbuzz(UInt32 start=1, UInt32 end=100)
        {
            bool fizz, buzz;
            while(start <= end)
            {
                fizz = (start % 3 == 0);
                buzz = (start % 5 == 0);
                if (fizz) Console.Write("Fizz");
                if (buzz) Console.Write("Buzz");
                if (!fizz && !buzz) Console.Write(start);
                Console.Write(' ');
                start++;
            }
        }
    }
}
