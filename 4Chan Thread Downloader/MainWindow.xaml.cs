﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace _4Chan_Thread_Downloader
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnDownload_Click(object sender, RoutedEventArgs e)
        {
            if (!Regex.IsMatch(tbUrl.Text, regex4chan))
            {
                MessageBox.Show("Invalid 4chan thread link");
                return;
            }

            try
            {
                Stream thread = client.OpenRead(tbUrl.Text);
                StreamReader threadReader = new StreamReader(thread);
                content = threadReader.ReadToEnd();
            }
            catch(WebException webEx) { MessageBox.Show(webEx.Message); return; }; 
            threadId = tbUrl.Text.Split('/').Last().Split('#').First();
            Directory.CreateDirectory(threadId);
            Parse(content);
            MessageBox.Show(string.Format("Done!{0}Path: {1}", Environment.NewLine, threadId));
        }

        private void Parse(string content)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(content);
            var threadDiv = doc.GetElementbyId("t"+threadId);
            
            string path = threadId + @"/content.html";
            //if (!File.Exists(path)) File.Create(path);
            File.WriteAllText(path, threadDiv.InnerHtml);
            //BackgroundWorker bw = new BackgroundWorker();
            //bw.DoWork += DownloadFiles;
            //bw.RunWorkerAsync(threadDiv);
            DownloadFiles(threadDiv);
        }

        private void DownloadFiles(HtmlNode div)
        {
            var elements = div.Descendants().Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("fileText"));
            foreach (var element in elements)
            {
                string url = element.SelectSingleNode("a").Attributes["href"].Value;
                tbInfo.Text += string.Format("Downloading file: {0}{1}", url.Split('/').Last(), Environment.NewLine);
                client.DownloadFile("http:" + url, threadId + "/" + url.Split('/').Last());
            }
        }

        private void DownloadFiles(object sender, DoWorkEventArgs e)
        {
            if (e.Argument is HtmlNode)
            {
                var div = e.Argument as HtmlNode;
                var elements = div.Descendants().Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("fileText"));
                foreach (var element in elements)
                {
                    string url = element.SelectSingleNode("a").Attributes["href"].Value;
                    tbInfo.Text += string.Format("Downloading file: {0}{1}", url.Split('/').Last(), Environment.NewLine);
                    client.DownloadFile("http:" + url, threadId + "/" + url.Split('/').Last());
                }
            }
            return;
        }

        readonly string regex4chan = @"(https?:\/\/)*(boards.4chan.org\/)([A-Za-z]+\/)(thread\/)([0-9])+([q#0-9]*)";
        string content = "";
        string threadId = "./";
        WebClient client = new WebClient();
    }
}
