﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace ProgrammingChallenge
{
    class Program
    {
        static void Main(string[] args)
        {
            FizzBuzz.fizzbuzz();
            Console.WriteLine();
            Console.WriteLine(ReverseString.reverse(".gnirts tseT"));
            Console.WriteLine(Factorial.factorial(5));
            Console.WriteLine(TriangleNumber.trianglenumber(4));
            Quine.quine();
            Console.Read();
        }
    }   
}
