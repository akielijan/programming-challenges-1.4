﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BigInt = System.Numerics.BigInteger;

namespace ProgrammingChallenge
{
    class Factorial
    {
        public static BigInt factorial(uint num)
        {
            BigInt result = 1;
            while(num > 1)
            {
                result *= num--;
            }
            return result;
        }
    }
}
