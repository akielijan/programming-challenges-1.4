﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProgrammingChallenge
{
    class TriangleNumber
    {
        public static int trianglenumber(int n)
        {
            return n * (n + 1) / 2;
        }
    }
}
