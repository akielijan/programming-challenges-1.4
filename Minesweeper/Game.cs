﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Minesweeper
{
    public class Game
    {
        public Game(uint x = 9, uint y = 9, uint mines = 10)
        {
            sizeX = x;
            sizeY = y;
            //create field with borders
            cells = new Cell[sizeX + 2, sizeY + 2];
            for (int i = 0; i < sizeX + 2; i++) 
            {
                for (int j = 0; j < sizeY + 2; j++)
                {
                    cells[i, j] = new Cell();
                    cells[i, j].MineClicked += LoseGame;
                    cells[i, j].CellClicked += PlayOn;
                }
            }
            this.mines = mines;
            fieldsLeft = sizeX * sizeY;

            CreateGamefield((int)sizeX, (int)sizeY);

            GameWon += WinGame;
        }

        private void WinGame(object sender, EventArgs e)
        {
            MessageBox.Show("You won!");
        }

        ~Game()
        {
            for (int i = 0; i < sizeX + 2; i++)
            {
                for (int j = 0; j < sizeY + 2; j++)
                {
                    cells[i, j] = new Cell();
                    cells[i, j].MineClicked -= LoseGame;
                    cells[i, j].CellClicked -= PlayOn;
                }
            }
        }

        public void Draw(Grid panel)
        {
            for (int i = 0; i < sizeY; i++) panel.RowDefinitions.Add(new RowDefinition());
            for (int i = 1; i <= sizeX; i++)
            {
                panel.ColumnDefinitions.Add(new ColumnDefinition());
                for (int j = 1; j <= sizeY; j++)
                {
                    Button btn = cells[i, j].CellButton;
                    Grid.SetRow(btn, j-1);
                    Grid.SetColumn(btn, i-1);
                    panel.Children.Add(btn);
                }
            }
        }

        private void PlayOn(object sender, EventArgs e)
        {
            if (--fieldsLeft == mines) OnGameWon(EventArgs.Empty);
        }

        private void LoseGame(object sender, EventArgs e)
        {
            MessageBox.Show("You land on the mine! End!");
        }

        public event EventHandler GameWon;

        protected virtual void OnGameWon(EventArgs e)
        {
            EventHandler handler = GameWon;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void CreateGamefield(int x=9, int y=9)
        {
            uint minesLeft = mines;
            Random r = new Random();
            ++x; ++y;
            while(minesLeft > 0)
            {
                int posx = r.Next(1, x);
                int posy = r.Next(1, y);
                if (cells[posx, posy].State != SquareState.MINE)
                {
                    cells[posx, posy].SetMine();
                    minesLeft--;
                    for (int i = -1; i <= 1; i++)
                    {
                        for (int j = -1; j <= 1; j++)
                        {
                            if (i == 0 && j == 0) continue;
                            cells[posx + i, posy + j].IncrementMinesAround();
                        }
                    }
                    //squares[posx - 1, posy - 1].IncrementMinesAround();
                    //squares[posx - 1, posy    ].IncrementMinesAround();
                    //squares[posx - 1, posy + 1].IncrementMinesAround();
                    //squares[posx    , posy - 1].IncrementMinesAround();
                    //squares[posx    , posy + 1].IncrementMinesAround();
                    //squares[posx + 1, posy - 1].IncrementMinesAround();
                    //squares[posx + 1, posy    ].IncrementMinesAround();
                    //squares[posx + 1, posy + 1].IncrementMinesAround();
                }

            }
        }

        public Cell[,] cells { get; protected set; }
        uint mines;
        uint sizeX;
        uint sizeY;
        uint fieldsLeft;
    }
}
