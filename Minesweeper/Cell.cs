﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Minesweeper
{
    public class Cell
    {
        public Cell(SquareState state = SquareState.EMPTY)
        {
            this.State = state;
            this.CellButton = new Button();
            this.CellButton.Click += checkState;
            this.ClickedOnMine = false;
            this.minesAround = 0;
        }
        

        public bool ClickedOnMine { get; protected set; }
        public SquareState State { get; protected set; }
        public Button CellButton { get; protected set; }
        public uint minesAround { get; protected set; }

        public Cell SetMinesAround(uint minesAround) { this.minesAround = minesAround; return this; }
        public Cell SetMine(SquareState state = SquareState.MINE) { this.State = state; return this; }
        public Cell IncrementMinesAround() { this.minesAround++; return this; }

        public event EventHandler MineClicked;
        public event EventHandler CellClicked;

        protected virtual void OnMineClicked(EventArgs e)
        {
            EventHandler handler = MineClicked;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected virtual void OnCellClicked(EventArgs e)
        {
            EventHandler handler = CellClicked;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void checkState(object sender, System.Windows.RoutedEventArgs e)
        {
            CellButton.IsEnabled = false;
            CellButton.Content = minesAround;
            ClickedOnMine = (State == SquareState.MINE);
            if (ClickedOnMine) OnMineClicked(EventArgs.Empty);
            else
            {
                CellButton.IsEnabled = false;
                CellButton.Content = minesAround;
                OnCellClicked(EventArgs.Empty);
            }
        }
        
    }

    public enum SquareState
    {
        EMPTY = 0, //also border
        MINE = 1
    }
}
