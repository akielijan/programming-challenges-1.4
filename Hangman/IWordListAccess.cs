﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman
{
    public interface IWordListAccess<T>
    {
        List<Models.Phrase> getWordList(T source);
    }
}
