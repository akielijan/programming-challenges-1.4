﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Hangman
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int fails = 0;
        Models.Phrase randomWord;
        public MainWindow()
        {
            InitializeComponent();
            tbGuess.CharacterCasing = CharacterCasing.Lower;
            tbGuessChar.CharacterCasing = CharacterCasing.Lower;

            JsonWordListManager wordManager = new JsonWordListManager();
            randomWord = wordManager.getRandomWord();
            StringBuilder sb = new StringBuilder("");
            for(int i=0; i<randomWord.phrase.Length; i++)
            {
                sb.Append("*");
            }
            label.Content = sb.ToString();
        }

        private void btnGuessChar_Click(object sender, RoutedEventArgs e)
        {
            char c = tbGuessChar.Text[0];
            int idx = 0;
            StringBuilder sb = new StringBuilder(label.Content as string);
            if (!randomWord.phrase.Contains(c))
            {
                fails++;
                if (fails == 5) label.Content = "You failed 5 times, end!";
                return;
            }
            while ((idx = randomWord.phrase.IndexOf(c, idx)) >= 0)
            {
                sb[idx++] = c;
            }
            if (sb.ToString() == randomWord.phrase)
            {
                label.Content = "You won";
                return;
            }
            else Console.WriteLine(string.Format("{0} != {1}", randomWord.phrase, sb.ToString()));
            label.Content = sb.ToString();
        }

        private void btnGuess_Click(object sender, RoutedEventArgs e)
        {
            if (tbGuess.Text == randomWord.phrase)
            {
                label.Content = "You won";
            }
            else fails++;
        }
    }
}
