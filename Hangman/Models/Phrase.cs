﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Models
{
    /// <summary>
    /// Model for game's phrases. May be extended by other fields, like category of the phrase.
    /// </summary>
    public class Phrase
    {
        public Phrase(string phrase = "")
        {
            this.phrase = phrase;
        }
        public string phrase { get; protected set; }
    }
}
