﻿using System.Collections.Generic;
using JSON = Newtonsoft.Json.JsonConvert;
using Hangman.Models;
using System;

namespace Hangman
{
    class JsonWordListManager : IWordListAccess<string>
    {
        public JsonWordListManager()
        {
            //..//wordlist = getWordList("wordlist.txt");
            wordlist = new List<Phrase>();
            wordlist.Add(new Phrase("test"));
            wordlist.Add(new Phrase("dog"));
            wordlist.Add(new Phrase("cat"));
            wordlist.Add(new Phrase("weather"));
        }
        public List<Phrase> getWordList(string source)
        {
            return JSON.DeserializeObject<List<Phrase>>(System.IO.File.ReadAllText(source));
        }

        public void saveWordList(string filename)
        {
            List<Phrase> templateWordList = new List<Phrase>();
            templateWordList.Add(new Phrase("test"));
            templateWordList.Add(new Phrase("dog"));
            templateWordList.Add(new Phrase("cat"));
            templateWordList.Add(new Phrase("weather"));
            System.IO.File.WriteAllText(filename, JSON.SerializeObject(templateWordList));
        }

        public Phrase getRandomWord()
        {
            return wordlist[new Random().Next(wordlist.Count)];
        }

        List<Phrase> wordlist;
    }
}
