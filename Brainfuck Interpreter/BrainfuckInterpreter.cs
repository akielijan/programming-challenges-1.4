﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brainfuck_Interpreter
{
    class BrainfuckInterpreter
    {
        public BrainfuckInterpreter(int bufferSize = 30000)
        {
            array = new char[bufferSize];
            currentIndex = 0;
            Array.Clear(array, 0, bufferSize);
        }
        public string Result { get; protected set; }
        public event EventHandler ReadKey;
        char[] array;
        int currentIndex;

        protected virtual void OnReadKey(EventArgs e)
        {
            EventHandler handler = ReadKey;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void Increment() { array[currentIndex]++; }
        private void Decrement() { array[currentIndex]--; }
        private void MoveForward() { currentIndex++; }
        private void MoveBackward() { currentIndex--; }
        private char GetChar() { return array[currentIndex]; }
        public void ReadChar(char c) { array[currentIndex] = c; }

        public void Interpret(string script)
        {
            for(int i=0; i<script.Length; i++)
            {
                switch(script[i])
                {
                    case '>': MoveForward(); break; 
                    case '<': MoveBackward(); break;
                    case '+': Increment(); break;
                    case '-': Decrement(); break;
                    case '.': Result += GetChar(); break;
                    case '[':
                        {
                            if (GetChar() == 0) //skip loop, there is nothing to do
                            {
                                int loop = 1;
                                while(loop > 0)
                                {
                                    i++;
                                    switch(script[i])
                                    {
                                        case ']': loop--; break;
                                        case '[': loop++; break;
                                        default: break;
                                    }
                                }
                            }
                            break;
                        }
                    case ']': //do actual loop
                        {
                            int loop = 1;
                            while (loop > 0)
                            {
                                i--;
                                switch (script[i])
                                {
                                    case ']': loop++; break;
                                    case '[': loop--; break;
                                    default: break;
                                }
                            }
                            i--;
                            break;
                        }
                    case ',': OnReadKey(EventArgs.Empty); break; //do readkey
                    default: break; //treat it as a comment
                }
            }
        }
    }
}
