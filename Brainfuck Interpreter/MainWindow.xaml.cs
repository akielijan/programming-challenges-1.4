﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Brainfuck_Interpreter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnInterpret_Click(object sender, RoutedEventArgs e)
        {
            string toInterpret = tbBF.Text;
            bf = new BrainfuckInterpreter();
            bf.ReadKey += ShowReadKeyForm;
            bf.Interpret(toInterpret);
            //MessageBox.Show(bf.Result);
            tbResult.Text = bf.Result;
        }

        private void ShowReadKeyForm(object sender, EventArgs e)
        {
            ReadKeyForm rkf = new ReadKeyForm();
            rkf.ShowDialog();
            char c = rkf.Result;
            bf.ReadChar(c);
        }

        BrainfuckInterpreter bf;
    }
}
