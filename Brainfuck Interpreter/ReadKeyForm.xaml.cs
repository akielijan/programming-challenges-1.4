﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Brainfuck_Interpreter
{
    /// <summary>
    /// Interaction logic for ReadKeyForm.xaml
    /// </summary>
    public partial class ReadKeyForm : Window
    {
        public ReadKeyForm()
        {
            InitializeComponent();
        }

        public char Result { get; protected set; }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Result = textBox.Text[0];
            this.Close();
        }
    }
}
