﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProgrammingChallenge
{
    class SevenSegmentDisplay
    {
        Character[] hourDigits = { new Character(), new Character() };
        Character separator = new Character(new char[]{'*', '*'});
        Character[] minuteDigits = { new Character(), new Character() };

        public static void showDate()
        {
            
        }
    }

    class Character
    {
        //  
        //  _   a
        // |_| fgb
        // |_| edc
        //  
        const char vertSeg = '|'; //or char 219
        const char horSeg = '_';
        const char empty = ' ';
        const char separator = ':';
        //public static readonly Character Separator = new Character(new char[] {empty, empty, empty, empty, empty, empty, separator });
        public static readonly Character Zero = new Character(new char[] { horSeg, vertSeg, vertSeg, horSeg, vertSeg, vertSeg, empty });
        public static readonly Character One = new Character(new char[] { empty, vertSeg, vertSeg, empty, empty, empty, empty });
        public static readonly Character Two = new Character(new char[] { horSeg, vertSeg, empty, horSeg, vertSeg, empty, horSeg });
        public Character()
        {
            this.segments = new char[7];
        }
        public Character(char[] segments)
        {
            this.segments = segments;
        }
        protected char[] segments;
    }

    class ASCIIClock
    {
    }
}
